/*
 * Copyright © 2019 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Authors:
 *	Olivier Fourdan <ofourdan@redhat.com>
 */

#include <sys/stat.h>
#include <sys/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <epoxy/egl.h>

int
main(int argc, char **argv)
{
    const GLubyte *renderer;
    EGLDisplay display;
    EGLContext context;
    int i;
    int status = EXIT_FAILURE;

    if (epoxy_has_egl_extension(EGL_DEFAULT_DISPLAY, "EGL_KHR_platform_base")) {
        display = eglGetPlatformDisplay(EGL_PLATFORM_GBM_MESA,
                                        EGL_DEFAULT_DISPLAY, NULL);
    }
    else if (epoxy_has_egl_extension(EGL_DEFAULT_DISPLAY, "EGL_EXT_platform_base")) {
        display = eglGetPlatformDisplayEXT(EGL_PLATFORM_GBM_MESA,
                                           EGL_DEFAULT_DISPLAY, NULL);
    }
    else {
        display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    }

    if (display == EGL_NO_DISPLAY) {
        fprintf(stderr, "Failed to get display\n");
        goto done;
    }

    if (eglInitialize(display, NULL, NULL) == EGL_FALSE) {
        fprintf(stderr, "Failed to initialize display\n");
        goto done;
    }

    context = eglCreateContext(display, NULL, EGL_NO_CONTEXT, NULL);
    if (context == EGL_NO_CONTEXT) {
        fprintf(stderr, "Failed to create context\n");
        goto done;
    }

    if (!eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, context)) {
        fprintf(stderr, "Failed to make context current\n");
        goto done;
    }

    renderer = glGetString(GL_RENDERER);
    if (!renderer) {
        fprintf(stderr, "Failed to get renderer\n");
        goto done;
    }

    fprintf(stdout, "%s\n", renderer);
    for (i = 1; i < argc; i++) {
        if (strstr((const char *)renderer, argv[i])) {
            fprintf(stdout, "%s found\n", argv[i]);
            status = EXIT_SUCCESS;
            break;
        }
    }
done:
    if (display != EGL_NO_DISPLAY) {
        eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglTerminate(display);
    }

    return status;
}
